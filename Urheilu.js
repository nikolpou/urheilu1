class Henkilo {
  constructor(etunimi, sukunimi, kutsumanimi, syntymavuosi) {
    this.etunimi = etunimi;
    this.sukunimi = sukunimi;
    this.kutsumanimi = kutsumanimi;
    this.syntymavuosi = syntymavuosi;
  }
}

class Urheilija extends Henkilo {
  constructor(
    etunimi,
    sukunimi,
    kutsumanimi,
    syntymavuosi,
    linkki,
    paino,
    laji,
    saavutukset
  ) {
    super(
      etunimi,
      sukunimi,
      kutsumanimi,
      syntymavuosi,
      linkki,
      paino,
      laji,
      saavutukset
    );

    this.linkki = linkki;
    this.paino = paino;
    this.laji = laji;
    this.saavutukset = saavutukset;
  }
  set linkki(value) {
    this._linkki = value;
  }
  get linkki() {
    return this._linkki;
  }
  naytaTiedot() {
    console.log(this.etunimi);
    console.log(this.sukunimi);
    console.log(this.laji);
    console.log(this.paino);
    console.log(this.saavutukset);
  }
}

var osmo = new Urheilija(
  "Osmo",
  "Pekkarinen",
  "Pepe",
  1989,
  "/img/pepe.jpg/",
  "82kg",
  "nyrkkeily",
  "pm.hopea1995"
);
osmo.linkki = "pepe_new.gif";
osmo.etunimi = "Rosmo";
osmo.paino = "55kg";
osmo.naytaTiedot();
